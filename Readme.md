# Juego del Kraft

OBJETIVO: Crear el juego de Kraft que tiene las siguietes reglas.

REGLAS:
Cuando se juega en un casino contra la casa, el jugador
es designado como �tirador� o �shooter� por su nombre
en ingl�s. Para comenzar el juego, durante lo que se
conoce como �tiro de salida�, el jugador busca obtener
un siete (conocido como �siete ganador�) o un once. Si
por el contrario obtiene un par de unos pierde
autom�ticamente. Si durante el primer lanzamiento no
obtiene un siete u once (con lo que ganar�a), o un dos (con
lo que perder�a), el juego entrar� en una segunda etapa,
en la que se registrar� las suma de los dados, siendo este
valor el OBETIVO.
En esta segunda etapa, el tirador buscar� volver a
obtener ese mismo valor en los dados (OBJETIVO), con lo
que ganar�a la partida, a no ser que obtenga un siete, con
lo cual, perder�a la partida.