package com.example.juegokraft;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	int valorDado1=0;
	int valorDado2=0;
	int suma = 0;
	int objetivo = 0;
	// si suma == 0 entonces el juego recien comienza
	//si suma == (7 || 11) entonces  suma = 999 y se gano el juego
	//si suma == -666 entonces  perdiste el juego
	public void tirarDados(){
		valorDado1 = (int ) (Math.random()*5 + 1);
		valorDado2 = (int ) (Math.random()*5 + 1);
		TextView dado1 = (TextView) findViewById(R.id.textView1);
		dado1.setText(valorDado1+"");
		TextView dado2 = (TextView) findViewById(R.id.textView2);
		dado2.setText(valorDado2+"");
	}
	public void verificar(View vista){
		if(suma == 0){
			
			tirarDados();

			TextView estado = (TextView) findViewById(R.id.textView3);
			
			suma = valorDado1 + valorDado2;
			if(suma == 7 || suma == 11){				
				estado.setText("Estado: ¡¡¡GANASTEEE!!!");
				suma=999;
			}else{
				objetivo = suma;
				estado.setText("Estado: Tira los dados una vez mas, debes sacar un "+ objetivo);
			}
		}
		if (suma > 0 && suma < 15){
			tirarDados();

			suma = valorDado1 + valorDado2;
			if(suma == 7){
				suma=-666;
				TextView estado = (TextView) findViewById(R.id.textView3);
				estado.setText("Estado: PERDISTE :(");
			}
			else{
				if(suma == objetivo){
					suma=999;
					TextView estado = (TextView) findViewById(R.id.textView3);
					estado.setText("Estado: ¡GANASTEEE! (tarde, pero ganaste)");
				}
				else{
					objetivo = suma;
					TextView estado = (TextView) findViewById(R.id.textView3);
					estado.setText("Estado: Tira los dados una vez mas, debes sacar un "+ objetivo);
				}
			}
		}
	}
	public void reiniciar(View vista){
		TextView estado = (TextView) findViewById(R.id.textView3);
		estado.setText("Estado: Tira los dados");
		suma = 0;
		TextView dado1 = (TextView) findViewById(R.id.textView1);
		dado1.setText("Dado1");
		TextView dado2 = (TextView) findViewById(R.id.textView2);
		dado2.setText("Dado2");
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
